###Determine whether adjecent markers are informative
#Mark Sterken, 26/7/2020

###input
#genotype.matrix (preferrably numeric matrix with genotypes; if not it will force it to be numberic)

###output
#a vector with whether a marker is informative (1) or not (0)

###Description
#performes side-by-side correlation analysis to determine if crossovers occured between markers. If so it will deem the markers on either side useful.

###See also                                         

genetic.informative.markers <- function(genotype.matrix){
                                         ###force into numeric matrix for correlation analysis
                                         genotype.matrix.numeric <- matrix(as.numeric(as.factor(unlist(genotype.matrix))),ncol=ncol(genotype.matrix))
                                             genotype.matrix.numeric[is.na(genotype.matrix.numeric)] <- 0
                                        
                                         tmp <- round(as.numeric(diag(cor(t(genotype.matrix.numeric))[-1,])),digits=5)
                                         ###each different marker is informative
                                         tmp1 <- c(0,tmp)
                                         tmp2 <- c(tmp,0)
                                         
                                         output <- tmp1 != 1 | tmp2 != 1
                                         return(output)
                                        }
