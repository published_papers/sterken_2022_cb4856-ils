###function for calling peaks in IL data
#Mark Sterken 6/7/2022

###input
#ILmap.dataframe; output of the IL mapping functions (both 1BG and 2BG)
#threshold; the significance treshold for calling QTL (in -log10(p))
#LOD.drop; the decrease in LOD-score compared to the peak to call the boundaries of the QTL. Standard is 1.5
#model.select; for 2BG mapping, standard is marker
#peakdist; minimum physical distance between two peaks (set at 1e6)

###output
#the original dataframe with the added columns: qtl_peak, qtl_marker_left, qtl_bp_left, qtl_marker_right, qtl_bp_right
#these columns are only non-NA where a QTL peak is located.

###Description
#This takes output of mapping.to.list function and identifies peaks and confidence intervals 
#Based on a given threshold and a 1.5 LOD-drop.



IL.peak.finder <- function(ILmap.dataframe,threshold,LOD.drop,model.select,peakdist){
                            if(missing(ILmap.dataframe)){      stop("Missing ILmap.dataframe (trait, qtl_chromosome, qtl_bp, qtl_marker, qtl_significance, qtl_effect)")}
                            if(missing(threshold)){            stop("Missing threshold")}
                            if(missing(LOD.drop)){             LOD.drop <- 1.5}
                            if(missing(peakdist)){             peakdist <- 1e6}

                            ###filer 2 BG data
                            if("qtl_model" %in% colnames(ILmap.dataframe)){
                                if(missing(model.select)){         model.select <- "marker";warning("model.select set to marker")}  
                                
                                ILmap.dataframe <- dplyr::filter(ILmap.dataframe,qtl_model == model.select)
                            }
    
                            ILuse.dataframe <- ILmap.dataframe
                            peaksout <- NULL
                            for(i in 1:4){           
                                ###locate the peak
                                peaks <- dplyr::filter(ILuse.dataframe, qtl_significance >= threshold-LOD.drop) %>%
                                         dplyr::group_by(trait,qtl_chromosome) %>%
                                         dplyr::filter(qtl_significance == max(qtl_significance),max(qtl_significance)>=threshold) %>%
                                         dplyr::filter(abs(qtl_marker - mean(qtl_marker)) == min(abs(qtl_marker - mean(qtl_marker)))) %>%
                                         dplyr::filter(qtl_marker == min(qtl_marker)) %>%
                                         as.data.frame() %>%
                                         dplyr::mutate(qtl_peak=qtl_marker)
    
                                ###locate left boundary
                                peaks_left <- dplyr::filter(ILuse.dataframe,trait %in% unique(peaks$trait),!is.na(qtl_significance)) %>%
                                              dplyr::group_by(trait,qtl_chromosome) %>%
                                              dplyr::mutate(Border=ifelse((qtl_significance < max(qtl_significance)-LOD.drop & max(qtl_significance)>=threshold),T,
                                                                     ifelse((qtl_bp == min(qtl_bp) & min(qtl_bp) %in% qtl_bp[qtl_significance >= max(qtl_significance)-LOD.drop & max(qtl_significance)>=threshold]),T,
                                                                       ifelse((qtl_bp == max(qtl_bp) & max(qtl_bp) %in% qtl_bp[qtl_significance >= max(qtl_significance)-LOD.drop & max(qtl_significance)>=threshold]),T,F))),
                                                            peakloc=qtl_marker[which.max(qtl_significance)]) %>%
                                              dplyr::filter(Border) %>%
                                              dplyr::mutate(qtl_marker_left=as.numeric(ifelse(qtl_marker == max(qtl_marker[qtl_marker <= peakloc]),qtl_marker,NA)),
                                                            qtl_bp_left=as.numeric(ifelse(qtl_bp == max(qtl_bp[qtl_marker <= peakloc]),qtl_bp,NA)),
                                                            qtl_marker_right=as.numeric(ifelse(qtl_marker == min(qtl_marker[qtl_marker >= peakloc]),qtl_marker,NA)),
                                                            qtl_bp_right=as.numeric(ifelse(qtl_bp == min(qtl_bp[qtl_marker >= peakloc]),qtl_bp,NA))) %>%
                                              dplyr::filter(!is.na(qtl_bp_left) | !is.na(qtl_bp_right)) %>%
                                              dplyr::select(-Border,-peakloc)
    
                                peaks_right <- dplyr::filter(peaks_left,!is.na(qtl_bp_right))
                                peaks_left <- dplyr::filter(peaks_left,!is.na(qtl_bp_left))
    
                                ###Merge the findings
                                peaks <- cbind(peaks,peaks_left$qtl_marker_left,peaks_left$qtl_bp_left,peaks_right$qtl_marker_right,peaks_right$qtl_bp_right)
                                
                                peaksout <- rbind(peaksout,peaks)
                                
                                if(dim(peaks)[1] != 0){
                                    for(j in 1:nrow(peaks)){
                                        ILuse.dataframe[ILuse.dataframe$trait==peaks$trait[j] & 
                                                        ILuse.dataframe$qtl_bp %in% (peaks[j,10]-peakdist):(peaks[j,12]+peakdist) &
                                                        ILuse.dataframe$qtl_chromosome == peaks[j,2],5:6] <- -1*LOD.drop
                                    }
                                }
                            }
                            
                            ###Prepare input
                            output <- dplyr::mutate(ILmap.dataframe,qtl_peak = NA,qtl_marker_left = NA,qtl_bp_left = NA,qtl_marker_right = NA,qtl_bp_right = NA)
                            ###Merge peaks and input
                            output[match(paste(peaksout[,1],peaksout[,2],peaksout[,3]),paste(output[,1],output[,2],output[,3])),8:12] <- peaksout[,8:12]
                            output <- dplyr::mutate(output,qtl_bp_left=ifelse(qtl_bp_left > qtl_bp,qtl_bp,qtl_bp_left),qtl_bp_right=ifelse(qtl_bp_right<qtl_bp,qtl_bp,qtl_bp_right))
                            
                            ###return called peaks
                            return(output)
                           }
