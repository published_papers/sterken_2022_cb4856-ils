    #################################################################################
    ###Load & clean data
    #################################################################################

        ###Genotypes (WN256 excluded as introgression was not detected)
            strain.map.tmp <- IL_mat[[1]][IL_mat[[2]]$informative_marker==1,]
                strain.map.tmp[strain.map.tmp=="CB4856"] <- "-1"
                strain.map.tmp[strain.map.tmp=="N2"] <- "1"
                strain.map <- matrix(as.numeric(strain.map.tmp),ncol=ncol(strain.map.tmp))
                    colnames(strain.map) <- colnames(strain.map.tmp)
                    rownames(strain.map) <- rownames(strain.map.tmp)
            strain.marker <- IL_mat[[2]][IL_mat[[2]]$informative_marker==1,]
        
            merger <-  filter(complete_set,(Strain_type=="IL_CB4856" | grepl("Same as ",Comments))) %>%
                       mutate(Strain=ifelse(grepl("Same as ",Comments),gsub("Same as ","",Comments),Strain)) %>%
                       mutate(Strain=gsub("WN337; introgression called manually after cross-referencing FLP map","WN337",Strain)) %>%
                       select(Strain_alternative_name,Strain) %>%
                       mutate(Strain_alternative_name=toupper(Strain_alternative_name)) %>%
                       rename(Strain_CBN = Strain_alternative_name, Strain_WN=Strain) %>%
                       filter(!duplicated(Strain_CBN)) 
                   
            merger <- filter(complete_set,(Strain_type=="IL_CB4856")) %>%
                      select(Strain_alternative_name,Strain) %>%
                      mutate(Strain_alternative_name=toupper(Strain_alternative_name)) %>%
                      rename(Strain_CBN = Strain_alternative_name, Strain_WN=Strain) %>%
                      filter(!duplicated(Strain_CBN)) 
                                      
            
    life_data <- read.xlsx("./Data/Reverse_IL_experiments.xlsx",sheet=2,detectDates=TRUE) %>%
                 rename(Strain_CBN=Genotype) %>%
                 merge(merger,all.x=TRUE,by.x=5,by.y=1) %>%
                 mutate(Strain_WN=ifelse(Strain_CBN=="CB4856","CB4856",
                                    ifelse(Strain_CBN=="N2","N2",Strain_WN))) %>%
                 group_by(plate_id) %>%
                 mutate(lifespan_mean=mean(lifespan),lifespan_median=median(lifespan),lifespan_var=var(lifespan),lifespan_min=min(lifespan),lifespan_max=max(lifespan)) %>%
                 data.frame()
                 
    save(life_data,file="./Data/obj_life_data.Rdata")
    
    
    