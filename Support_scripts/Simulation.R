###simulation script
    simulate.phenotype <- function(QTL_n=1:100,sd_trait=1,trait_n=10000,effect_size_distribution="normal",marker_location_distribution="random",
                                   strain.map,strain.marker,markers_close_n=NA,closely_linked_effects="no_effect",QTL_model="additive",reps=10,heritability=0.8){
                                    if(!effect_size_distribution %in% c("normal","random")){                                         stop("effect_size-distribution is either normal or random")}
                                    if(!marker_location_distribution %in% 
                                       c("random","closely_linked","random_interacting","closely_linked_interacting")){              stop("marker_location_distribution is either random, random_interacting, closely_linked, or closely_linked_interacting")}
                                    if(missing(strain.marker)){                                                                      stop("requires strain.marker, name, chromosome, position")}
                                    if(grepl("closely_linked",marker_location_distribution) & is.na(markers_close_n)){               stop("when simulating a closely linked architecture, provide how many markers")}
                                    if(grepl("interacting",marker_location_distribution)){                                           QTL_model <- "interacting"; closely_linked_effects <- "interacting"}
                                    if(!closely_linked_effects %in% c("no_effect","balancing","interacting")){                       stop("closely_linked_effects is either no_effect, balancing, or interacting")}
                                    if(QTL_model == "interacting" & !grepl("interacting",marker_location_distribution)){             stop("when choosing an interacting QTL model, the marker distribution should be either random_interacting or closely_linked_interacting")}
                                    if(!QTL_model %in% c("additive","interacting","genotype_dependent_additive")){                   stop("QTL_model is either additive, interacting, or genotype_dependent_additive")}
                                    ###Effects: normal                          |                                          random
                                    ###Markers: random |   closely_linked       | random_interacting | closely_linked_interacting
                                    ###effxm:             balancing|no_effect                      interacting
                                    ###QTL:     additive | genotype_dependent_additive          interacting
                                    
                                    
                                    output <- list(NULL)
                                    output[[1]] <- NULL
                                    out <- NULL
                                    for(j in QTL_n){
                                        output.tmp <- list(NULL)
                                        out.tmp <- NULL
                                        for(i in 1:trait_n){
                                            tmp <- list(NULL)
                                            ###effects
                                            eff <- simulate.effect.distribution(effect_size_distribution=effect_size_distribution,effects_n=j,sd_trait=sd_trait)
                                            ###Marker
                                            mrk <- simulate.marker.distribution(marker_location_distribution=marker_location_distribution,markers_n=j,strain.marker=strain.marker,markers_close_n=,markers_close_n)
                                            ###Marker x effect
                                            if(grepl("closely",marker_location_distribution)){
                                                eff <- simulate.effect.distribution(effect_size_distribution=effect_size_distribution,effects_n=j*markers_close_n,sd_trait=sd_trait)   
                                                eff <- simulate.closely.linked(closely_linked_effects=closely_linked_effects,effects=eff,markers=mrk,markers_close_n=markers_close_n)
                                            }

                                            phe <- simulate.phenotypes(QTL_model=QTL_model,effects=eff,markers=mrk,strain.map=strain.map)
                                                rowphe <- rownames(phe)
                                            phe <- matrix(phe,ncol=reps,nrow=length(phe))
                                                
                                                
                                            ###error model
                                            add.noise <- function(x,heritability){
                                                                  varRIL <- var(as.numeric(x))
                                                                  
                                                                  sdE <- ((varRIL-(heritability*varRIL))/heritability)^0.5
                                                                  
                                                                  return(x + rnorm(length(x),sd=sdE))
                                                                 }                                            
                                            
                                            phe <- add.noise(phe,heritability = heritability)
                                                rownames(phe) <- rowphe
                                                
                                            ###output: phe[[1]] is ILs, phe[[2]] RILs
                                            if(length(eff)/length(mrk) == 1){tmp[[1]] <- cbind(effects=eff,marker=mrk)}
                                            if(length(eff)/length(mrk) ==0.5){tmp[[1]] <- cbind(effects=eff,marker1=mrk[,1],marker2=mrk[,2])}
                                            tmp[[2]] <- phe
                                            
                                            names(tmp) <- c("effect_marker","phenotypes")
                                            
                                            output.tmp[[i]] <- tmp
                                        }
                                        names(output.tmp) <- paste("simulation",1:trait_n,sep="_")
                                        output[[j]] <- output.tmp
                                    }
                                    return(output)
                                   }
 
    ###Add some noise
    simulate.trait.var.noise <- function(simulate.trait.var.output,heritability){
                                        add.noise <- function(x,heritability){
                                                              varRIL <- var(x[[2]])
                                                              
                                                              sdE <- ((varRIL-(heritability*varRIL))/heritability)^0.5
                                                              
                                                              return(var(x[[2]]+rnorm(length(x[[2]]),sd=sdE))/var(x[[3]]+rnorm(length(x[[3]]),sd=sdE)))
                                                             }
                                         
                                         output <- simulate.trait.var.output[[1]]
                                         for(i in 2:nrow(simulate.trait.var.output[[1]])){
                                            index <- which(unlist(lapply(simu,length))[-1]!=0)[i]
                                            output[i-1,] <- unlist(lapply(simulate.trait.var.output[[index]],add.noise,heritability=heritability))
                                         }
                                         return(output)
                                        }


     simulate.effect.distribution <- function(effect_size_distribution,effects_n,sd_trait){
                                            ###normal distribution
                                            if(effect_size_distribution=="normal"){
                                                eff <- rnorm(effects_n,mean=0,sd=sd_trait) 
                                            }
                                            ###random numbers: QTL with same effect sizes
                                            if(effect_size_distribution=="random"){
                                                eff <- runif(effects_n,-2*sd_trait,2*sd_trait)
                                            }
                                            return(eff)
                                           }

                                   
    simulate.marker.distribution <- function(marker_location_distribution,markers_n,strain.marker,markers_close_n=NA){
                                            
                                            ###Markers random
                                            if(marker_location_distribution=="random"){
                                                mrk <- round(runif(markers_n,1,nrow(strain.marker))) 
                                            }
       
                                            ###Markers interact
                                            if(marker_location_distribution=="random_interacting"){
                                                mrk1 <- round(runif(markers_n,1,nrow(strain.marker))) 
                                                mrk2 <- round(runif(markers_n,1,nrow(strain.marker))) 
                                                
                                                mrk <- cbind(mrk1,mrk2)
                                            }    
       
                                            ###Markers closely linked
                                            if(marker_location_distribution=="closely_linked"){
                                                mrk <- rep(round(runif(markers_n,1,nrow(strain.marker))),each=markers_close_n)
                                                ###some spread
                                                #mrk <- mrk+round(runif(markers_n*markers_close_n,-1.5,1.5))
                                                mrk <- mrk+round(rnorm(markers_n*markers_close_n,0,1.5))
                                                mrk[mrk>nrow(strain.marker)] <- nrow(strain.marker)
                                                mrk[mrk<1] <- 1
                                                ###check chromosome
                                                awesome <- function(x,strain.marker){ifelse(length(unique(strain.marker[x,2]))==1,return(x),return(rep(round(median(x)),times=length(x))))                                          }
                                                mrk <- tapply(mrk,rep(1:markers_n,each=markers_close_n),awesome,strain.marker=strain.marker)
                                                mrk <- as.numeric(as.character(unlist(mrk)))
                                            }
                                            
                                            ###Markers closely linked interacting
                                            if(marker_location_distribution=="closely_linked_interacting"){
                                                mrk1 <- rep(round(runif(markers_n,1,nrow(strain.marker))),each=markers_close_n)
                                                ###some spread
                                                #mrk1 <- mrk1+round(runif(markers_n*markers_close_n,-1.5,1.5))
                                                mrk1 <- mrk1+round(rnorm(markers_n*markers_close_n,0,1.5))
                                                mrk1[mrk1>nrow(strain.marker)] <- nrow(strain.marker)
                                                mrk1[mrk1<1] <- 1
                                                ###check chromosome
                                                awesome <- function(x,strain.marker){ifelse(length(unique(strain.marker[x,2]))==1,return(x),return(rep(round(median(x)),times=length(x))))                                          }
                                                mrk1 <- as.numeric(as.character(unlist(tapply(mrk1,rep(1:markers_n,each=markers_close_n),awesome,strain.marker=strain.marker))))
                                                ###complete the pair
                                                awesome <- function(x){return(x[order(runif(length(x)))])}
                                                mrk2 <- as.numeric(as.character(unlist(tapply(mrk1,rep(1:markers_n,each=markers_close_n),awesome))))
                                                
                                                mrk <- cbind(mrk1,mrk2)
                                            }
        
                                            return(mrk)
                                           } 
        
    simulate.closely.linked <- function(closely_linked_effects,effects,markers,markers_close_n){
                                        
                                        ###effects not affected by localization
                                        if(closely_linked_effects == "no_effect"){
                                            return(effects)
                                        }
                                        if(closely_linked_effects == "balancing"){
                                            corfac <- rep(tapply(effects,rep(1:(length(effects)/markers_close_n),each=markers_close_n),mean),each=markers_close_n)
                                            effects <- (effects/corfac)-1
                                            return(effects)
                                        }
                                        if(closely_linked_effects == "interacting"){
                                            return(effects)
                                        }
                                       }                                    

    simulate.phenotypes <- function(QTL_model,effects,markers,strain.map){
                                    
                                    ###simple additive
                                    if(QTL_model=="additive"){
                                        if(length(markers)==1){
                                            phe <- strain.map[markers,]*effects
                                        }
                                        if(length(markers)>1){
                                            phe <- t(strain.map[markers,])%*%effects
                                        }
                                    }
                                    ###Interaction model
                                    if(QTL_model=="interacting"){
                                        if(nrow(markers)==1){
                                            phe <- strain.map[markers[,1],]*effects + strain.map[markers[,2],]*effects
                                        }
                                        if(nrow(markers)>1){
                                            phe <- t(strain.map[markers[,1],])%*%effects + t(strain.map[markers[,2],])%*%effects
                                        }
                                    }
        
                                    ###additive, but genotype dependent
                                    if(QTL_model=="genotype_dependent_additive"){
                                        dependency <- sign(runif(length(effects),-1,1))
                                        map.tmp <- strain.map[markers,]* dependency
                                        map.tmp[map.tmp < 0] <- 0

                                        if(length(markers)==1){
                                            phe <- map.tmp*effects
                                        }
                                        if(length(markers)>1){
                                            phe <- t(map.tmp)%*%effects
                                        }
                                    }
        
                                    phe <- matrix(phe,ncol=1)
                                    rownames(phe) <- colnames(strain.map)
        
                                    return(phe)
                                   }
                                        
